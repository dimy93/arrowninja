package myExample;
import java.util.List;

import org.mt4j.components.MTComponent;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import processing.core.PApplet;
import processing.core.PGraphics;


public class BowString extends MTComponent{
	List<Vector3D> points;
	Vector3D position = new Vector3D(0,0,0);
	int color = 0;
	final int segmentCount;
    public BowString(PApplet mtApplication,List<Vector3D> points, int segmentCount){
        super(mtApplication);
        this.points = points;
        this.segmentCount = segmentCount;
    }
    
    public BowString(PApplet mtApplication,List<Vector3D> points){
        this(mtApplication,points,(int)(points.get(0).x - points.get(1).x));
    }

    private float interFunc( float x ){
    	 float result = 0,lj;
    	 for ( Vector3D p : points ){
    		 lj=1;
    		 for ( Vector3D nonp : points ){
    			 if ( p == nonp )
    				 continue;
    			 lj *= (x - nonp.x)/(p.x-nonp.x);
    		 }
    		 result += p.y*lj;
    	 }
    	 return result;
    }
    public void setStrokeColor(MTColor color){
    	int b = (int)(color.getB());
    	int r = (int)(color.getR()) << 16;
    	int g = (int)(color.getG()) << 8;
    	int a = (int)(color.getAlpha()) << 24;
    	this.color = b | r | g | a;
    } 
    public void setPositionGlobal( Vector3D v ){
    	position = v;
    }
    @Override
    public void drawComponent(PGraphics g){
    	g.strokeWeight= 10;
    	g.stroke(color);
        Vector3D start = points.get(0);
        Vector3D end = points.get(points.size()-1);
        float step = (end.x-start.x)/100f;
        for ( float x = start.x + step; x < end.x; x += step ){
        	g.line(position.x + x-step, position.y + interFunc(x-step), position.x + x, position.y + interFunc(x));
        }
    }
}