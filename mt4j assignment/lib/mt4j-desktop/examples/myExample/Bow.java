package myExample;

import java.util.ArrayList;
import java.util.List;


import org.mt4j.components.MTComponent;
import org.mt4j.components.interfaces.IMTComponent3D;
import org.mt4j.components.visibleComponents.shapes.AbstractShape;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.input.IMTInputEventListener;
import org.mt4j.input.gestureAction.DefaultDragAction;
import org.mt4j.input.gestureAction.DefaultRotateAction;
import org.mt4j.input.gestureAction.DefaultScaleAction;
import org.mt4j.input.inputData.AbstractCursorInputEvt;
import org.mt4j.input.inputData.InputCursor;
import org.mt4j.input.inputData.MTInputEvent;
import org.mt4j.input.inputProcessors.MTGestureEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.DragEvent;
import org.mt4j.input.inputProcessors.componentProcessors.dragProcessor.MultipleDragProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.rotateProcessor.RotateEvent;
import org.mt4j.input.inputProcessors.componentProcessors.rotateProcessor.RotateProcessor;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleEvent;
import org.mt4j.input.inputProcessors.componentProcessors.scaleProcessor.ScaleProcessor;
import org.mt4j.util.MTColor;
import org.mt4j.util.math.Vector3D;

import processing.core.PApplet;
import processing.core.PImage;

public class Bow extends MTRectangle{
	private BowString string;
	private PApplet context;
	private float rotationZ = 0f, lastRotAngle = 0f;

	private float width = 1, height = 1;
	
	public final static float scale = 2f;
	private final static float stringOffsetX = 150/scale;
	private final static float maxStringOffsetY = 210/scale;
	private final static float minStringOffsetY = 50/scale;
	private boolean resetRotation = false;
	
	protected Arrow arrow;
	
	public Bow(PApplet applet) {
		super(applet,0, 0, 1, 1);
		context = applet;
		
		//Bow image init
		final PImage bowImg = applet.loadImage(BowScene.imagesPath+"bow.png");
        setSizeLocal(bowImg.width/scale,bowImg.height/scale);
        setVisible(false);
        setTextureEnabled(true);
        setStrokeColor(new MTColor(0, 0, 0, 0));
        setTexture(bowImg);
        
        //Unregister touch
        unregisterAllInputProcessors(); 
        removeAllGestureEventListeners();
        
        //Bow string init
        final ArrayList<Vector3D> points = new ArrayList<Vector3D>();
        float cenX = getCenterPointLocal().x;
		float cenY = getCenterPointLocal().y;
        points.add(new Vector3D(cenX-stringOffsetX,cenY+minStringOffsetY,0));
        points.add(new Vector3D(cenX,cenY+minStringOffsetY,0));
        points.add(new Vector3D(cenX+stringOffsetX,cenY+minStringOffsetY,0));
        string = new BowString(applet,points);
        string.setStrokeColor(new MTColor(215,24,232));
        addChild(string);
        
        //Arrow init
        arrow = new Arrow(applet);
        Vector3D arrowPos = new Vector3D(0,0,0);
        arrowPos.addLocal(string.points.get(1));
        arrowPos.addLocal(new Vector3D(0, -arrow.getHeight()/2, 0));
        arrow.setPositionRelativeToParent(arrowPos);
		addChild(arrow);
	}
	
	protected void addDrag(MTComponent parent){
		final MultipleDragProcessor dragproc = new MultipleDragProcessor(context);
        DefaultDragAction dragaction = new DefaultDragAction(this){
        	@Override
        	protected void translate(IMTComponent3D comp, DragEvent de){
        		List<InputCursor> cursors = dragproc.getCurrentComponentCursors();
        		float min = Float.MAX_VALUE;
        		InputCursor minC = null;
        		for ( InputCursor c : cursors ){
        			if ( min > c.getCurrentEvtPosY()){
        				min = c.getCurrentEvtPosY();
        				minC = c;
        			}
        		}
        		if (minC == de.getDragCursor())
        		{
        			super.translate(comp, de);
        		}
        	}
        };
        
        parent.registerInputProcessor(dragproc);
        parent.addGestureListener(dragproc.getClass(), dragaction);
        this.registerInputProcessor(dragproc);
        this.addGestureListener(dragproc.getClass(), dragaction);
	}
	protected void addRotate(MTComponent parent){
		 //Rotation
        final RotateProcessor rotproc = new RotateProcessor(context);
        final DefaultRotateAction rotaction = new DefaultRotateAction(this){
        	@Override
        	protected void doAction(IMTComponent3D comp, RotateEvent re){
        		Vector3D f = new Vector3D(re.getFirstCursor().getCurrentEvtPosX(),re.getFirstCursor().getCurrentEvtPosY(),0);
        		Vector3D s = new Vector3D(re.getSecondCursor().getCurrentEvtPosX(),re.getSecondCursor().getCurrentEvtPosY(),0);
        		if ( f.y > s.y ){
        			Vector3D temp = f;
        			f = s;
        			s = temp;
        		}
        		if ( resetRotation ){
        			rotateZGlobal(getCenterPointGlobal(), -rotationZ);
        			rotationZ = 0;
        			resetRotation = false;
        		}
        		s.subtractLocal(f);
        		lastRotAngle = (float)(Math.atan2(s.y,s.x)-Math.atan2(1,0));
        		lastRotAngle = (float) Math.toDegrees(lastRotAngle);
        		
        		lastRotAngle -= rotationZ;
        		re.setRotationDegrees(lastRotAngle);
        		re.setRotationPoint(getCenterPointGlobal());
        		rotationZ = rotationZ + lastRotAngle;
        		super.doAction(comp, re);
        	}
        	@Override
        	public boolean processGestureEvent(MTGestureEvent g){
        		if(g.getId() == MTGestureEvent.GESTURE_ENDED || g.getId() == MTGestureEvent.GESTURE_CANCELED ){
        			resetRotation = true;
        		}
        		return super.processGestureEvent(g);
        	}
        };
        parent.registerInputProcessor(rotproc);
        parent.addGestureListener(RotateProcessor.class, rotaction);

        this.registerInputProcessor(rotproc);
        this.addGestureListener(RotateProcessor.class, rotaction);
	}
	
	protected void addScale(MTComponent parent){
		 //Rotation
       final ScaleProcessor scaleproc = new ScaleProcessor(context);
		final DefaultScaleAction scaleaction = new DefaultScaleAction(this) {
			final Vector3D f = new Vector3D(0, 0, 0);
			final Vector3D s = new Vector3D(0, 0, 0);
			
			float lastScale = -1f;
			
			Vector3D arrowPos = new Vector3D(0,0,0);
			
			final static float maxSpan = 550f;
			final static float deadzoneSpan = 150f;

			@Override
			protected void doAction(IMTComponent3D comp, ScaleEvent re) {
				f.x = re.getFirstCursor().getCurrentEvtPosX();
				f.y = re.getFirstCursor().getCurrentEvtPosY();
				s.x = re.getSecondCursor().getCurrentEvtPosX();
				s.y = re.getSecondCursor().getCurrentEvtPosY();
				f.subtractLocal(s);
				float scaleFactor = f.length();
				scaleFactor -= deadzoneSpan;
				scaleFactor = scaleFactor < 0 ? 0 : scaleFactor;
				scaleFactor = scaleFactor > maxSpan ? maxSpan : scaleFactor;
				scaleFactor /= maxSpan;
				lastScale = scaleFactor;
				string.points.get(1).y = getCenterPointLocal().y + minStringOffsetY
						+ (maxStringOffsetY - minStringOffsetY) * scaleFactor;
				arrowPos.x = arrowPos.y = arrowPos.z = 0;
    	        arrowPos.addLocal(string.points.get(1));
    	        arrowPos.y -= arrow.getHeight()/2;
    	        arrow.setPositionRelativeToParent(arrowPos);
			}
			@Override
        	public boolean processGestureEvent(MTGestureEvent g){
        		if(g.getId() == MTGestureEvent.GESTURE_ENDED || g.getId() == MTGestureEvent.GESTURE_CANCELED ){
        			arrow.fire(lastScale > 0.3f ? lastScale : 0.3f);
        			string.points.get(1).y = getCenterPointLocal().y + minStringOffsetY;
        			arrow = new Arrow(context);
        			arrowPos.x = arrowPos.y = arrowPos.z = 0;
        	        arrowPos.addLocal(string.points.get(1));
        	        arrowPos.y -= arrow.getHeight()/2;
        	        arrow.setPositionRelativeToParent(arrowPos);
        			addChild(arrow);
        		}
        		return super.processGestureEvent(g);
        	}
		};
       parent.registerInputProcessor(scaleproc);
       parent.addGestureListener(ScaleProcessor.class, scaleaction);
       this.registerInputProcessor(scaleproc);
       this.addGestureListener(ScaleProcessor.class, scaleaction);
	}
	
	
	protected void addShowing( final MTComponent parent ){
		parent.addInputListener(new IMTInputEventListener() {
			private int count = 0;
			Vector3D initPos = new Vector3D(0,0,0);
			public boolean processInputEvent(MTInputEvent inEvt) {
				if (inEvt instanceof AbstractCursorInputEvt) {
					final AbstractCursorInputEvt touch = (AbstractCursorInputEvt) inEvt;
					if (touch.INPUT_STARTED == touch.getId()) {
						if (count == 0) {
							setVisible(true);
							setRotation(0);
							resetRotation = false;
							if ( parent instanceof AbstractShape )
								initPos.x = ((AbstractShape) parent).getCenterPointGlobal().x;
								initPos.y = context.getHeight()-maxStringOffsetY;
								setPositionGlobal(initPos);
						}
						count += 1;
					}
					if (touch.INPUT_ENDED == touch.getId()) {
						count -= 1;
						if (count == 0)
							setVisible(false);
					}
				}
				return false;
			}
		});
	}
	
	@Override
	protected void setParent(MTComponent parent){
		super.setParent(parent);
		addDrag(parent);
		addRotate(parent);
		addShowing(parent);
		addScale(parent);
	}
	
	public void setRotation( float deg ){
		rotateZGlobal(getCenterPointGlobal(), deg-rotationZ);
		rotationZ = deg;
	}
	
	public float getRotationZLocal(){
		return rotationZ;
	}
	@Override
	public void setSizeLocal( float w, float h ){
		super.setSizeLocal(w, h);
		width = w;
		height = h;
	}
	
	public float getWidth(){
		return width;
	}
	public float getHeight(){
		return height;
	}
	
}

