package myExample;

import org.mt4j.MTApplication;

public class myExampleApp extends MTApplication{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;


	public static void main(String args[]){
		initialize();
	}
	
	@Override
	public void startUp(){
		//this.addScene(new FluidSimulationScene(this, "Fluid scene"));
		this.addScene(new BowScene(this, "Bow scene"));
	}
}
