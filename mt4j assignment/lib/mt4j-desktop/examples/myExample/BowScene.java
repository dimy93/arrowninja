package myExample;

import java.util.ArrayList;

import org.jbox2d.collision.AABB;
import org.jbox2d.collision.shapes.Shape;
import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.ContactListener;
import org.jbox2d.dynamics.World;
import org.jbox2d.dynamics.contacts.ContactPoint;
import org.jbox2d.dynamics.contacts.ContactResult;
import org.mt4j.AbstractMTApplication;
import org.mt4j.components.MTComponent;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.components.visibleComponents.widgets.MTTextField;
import org.mt4j.input.inputProcessors.globalProcessors.CursorTracer;
import org.mt4j.sceneManagement.AbstractScene;
import org.mt4j.sceneManagement.IPreDrawAction;
import org.mt4j.util.MTColor;
import org.mt4j.util.font.FontManager;
import org.mt4j.util.font.IFont;
import org.mt4j.util.math.ToolsMath;
import org.mt4j.util.math.Vector3D;

import processing.core.PImage;
import advanced.physics.physicsShapes.IPhysicsComponent;
import advanced.physics.physicsShapes.PhysicsCircle;
import advanced.physics.physicsShapes.PhysicsRectangle;
import advanced.physics.util.UpdatePhysicsAction;

public class BowScene extends AbstractScene {

	private float timeStep = 1.0f / 60.0f;
	private int constraintIterations = 10;

	/** THE CANVAS SCALE **/
	public static float scale = 20;
	private AbstractMTApplication app;
	public static World world;

	public static MTComponent physicsContainer;

	public static String imagesPath = "myExample"
			+ AbstractMTApplication.separator + "data"
			+ AbstractMTApplication.separator;
	
	private int lives = 3;
	private int score = 0;
	final private String scoreStr = "Score: ";
	final private String livesStr = "      Lives: ";

	public BowScene(final AbstractMTApplication mtApplication, String name) {
		super(mtApplication, name);
		
		// Background init
		final MTRectangle background = new MTRectangle(mtApplication, 0, 0,
				getCanvas().getRenderer().getWidth(), getCanvas().getRenderer()
						.getHeight());
		background.setFillColor(new MTColor(0, 0, 0));
		background.getCenterPointGlobal();

		background.unregisterAllInputProcessors();
		background.removeAllGestureEventListeners();
		getCanvas().addChild(background);
		
		
		//Add score field
		IFont font = FontManager.getInstance().createFont(mtApplication, "arial.ttf", 20,new MTColor(255,255,255));
        final MTTextField textField = new MTTextField(mtApplication,0,0,230,40,font);
        textField.setFillColor(new MTColor(0,0,0,0));
        textField.setText(scoreStr + score + livesStr + lives);
        background.addChild(textField);
		
		// Physics Init
		app = mtApplication;
		float worldOffset = 10; // Make Physics world slightly bigger than
								// screen borders
		
		// Physics world dimensions
		AABB worldAABB = new AABB(new Vec2(-worldOffset, -worldOffset),
				new Vec2((app.width) / scale + worldOffset, (app.height)
						/ scale + worldOffset));
		Vec2 gravity = new Vec2(0, 10);
		// Vec2 gravity = new Vec2(0, 0);
		boolean sleep = true;
		// Create the pyhsics world
		world = new World(worldAABB, gravity, sleep);

		this.registerGlobalInputProcessor(new CursorTracer(app, this));

		// Update the positions of the components according the the physics
		// simulation each frame
		this.registerPreDrawAction(new UpdatePhysicsAction(world, timeStep,
				constraintIterations, scale));

		physicsContainer = new MTComponent(app);
		// Scale the physics container. Physics calculations work best when the
		// dimensions are small (about 0.1 - 10 units)
		// So we make the display of the container bigger and add in turn make
		// our physics object smaller
		physicsContainer.scale(scale, scale, 1, Vector3D.ZERO_VECTOR);
		this.getCanvas().addChild(physicsContainer);

		// Create borders around the screen
		//this.createScreenBorders(physicsContainer);
		

		final Bow bow = new Bow(mtApplication);
		background.addChild(bow);
		
		this.registerPreDrawAction(new IPreDrawAction() {
			private int randomSleep;
			private final PImage watermellon = mtApplication.loadImage(imagesPath+"watermelon.png");
			{
				watermellon.resize(60,60);
			}
			private final PImage bomb = mtApplication.loadImage(imagesPath+"bomb3.png");
			{
				bomb.resize(60,60);
			}
			private final MTColor transperent =  new MTColor(0, 0, 0, 0);
			
			@Override
			public void processAction(){
				if ( randomSleep > 0 )
				{
					randomSleep --;
					return;
				}
				boolean left = ToolsMath.getRandom(0f, 1f) > 0.5f;
				Vector3D position = new Vector3D( left ? 0f : 1024f, ToolsMath.getRandom(0f, 450f));
				PhysicsCircle c = new PhysicsCircle(app, position, 50,
						world, 1.0f, 0.3f, 0.4f, scale);
				
				boolean good = ToolsMath.getRandom(0f, 1f) > 0.5f;
				c.setName(good?"watermellon":"bomb");
		        c.setTextureEnabled(true);
		        c.setStrokeColor(transperent);
		        
		        c.setTexture(good ? watermellon : bomb);
		        
				physicsContainer.addChild(c);
				float impulseX = ToolsMath.getRandom(250,500);
				Vec2 impulse = new Vec2(left ? impulseX : -impulseX,-100);
				c.getBody().applyImpulse(impulse.clone(), c.getBody().getPosition() );
				randomSleep = (int) ToolsMath.getRandom(10f, 60f);
				
			}
			
			@Override
			public boolean isLoop() {
				// TODO Auto-generated method stub
				return true;
			}
		});
		world.setContactListener(new ContactListener(){

			@Override
			public void add(ContactPoint arg0) {
				// TODO Auto-generated method stub
				ArrayList<Shape> bomb = new ArrayList<Shape> ();
				ArrayList<Shape> watermellon = new ArrayList<Shape> ();
				ArrayList<Shape> arrow = new ArrayList<Shape> ();
				ArrayList<MTComponent> bombC = new ArrayList<MTComponent> ();
				ArrayList<MTComponent> watermellonC = new ArrayList<MTComponent> ();
				ArrayList<MTComponent> arrowC = new ArrayList<MTComponent> ();
				for ( MTComponent m : physicsContainer.getChildren()){
					IPhysicsComponent ipc = ((IPhysicsComponent)m);
					Shape sh = ipc.getBody().m_shapeList;
					if ( m.getName().equals("bomb") ){
						bomb.add(sh);
						bombC.add(m);
					}
					else if ( m.getName().equals("watermellon") ){
						watermellon.add(sh);
						watermellonC.add(m);
					}
					else if ( m.getName().equals("arrow") ){
						arrow.add(sh);
						arrowC.add(m);
					}
				}
				if ( arrow.contains(arg0.shape1) || arrow.contains(arg0.shape2) )
				{
					int bomb1 = bomb.indexOf(arg0.shape1);
					int bomb2 = bomb.indexOf(arg0.shape2);
					if ( bomb1 >= 0  || bomb2 >= 0 ) 
					{
						lives--;
						if ( lives < 0 )
							System.out.println("Dead");
					}
					if ( bomb1 >= 0 )
					{
						MTComponent comp = bombC.get(bomb1);
						comp.getParent().removeChild(comp);
					}
					if ( bomb2 >= 0 )
					{
						MTComponent comp = bombC.get(bomb2);
						comp.getParent().removeChild(comp);
					}
					
					int watermellon1 = watermellon.indexOf(arg0.shape1);
					int watermellon2 = watermellon.indexOf(arg0.shape2);
					if ( watermellon1 >= 0 || watermellon2 >= 0  ) 
					{
						score++;
					}
					if ( watermellon1 >= 0 )
					{
						MTComponent comp = watermellonC.get(watermellon1);
						comp.getParent().removeChild(comp);
					}
					if ( watermellon2 >= 0 )
					{
						MTComponent comp = watermellonC.get(watermellon2);
						comp.getParent().removeChild(comp);
					}
					textField.setText(scoreStr + score + livesStr + ((lives < 0) ? "Dead" : lives));
				}
			}

			@Override
			public void persist(ContactPoint arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void remove(ContactPoint arg0) {
				// TODO Auto-generated method stub
				
			}

			@Override
			public void result(ContactResult arg0) {
				// TODO Auto-generated method stub
				
			}});
	};

	private void createScreenBorders(MTComponent parent) {
		// Left border
		float borderWidth = 50f;
		float borderHeight = app.height;
		Vector3D pos = new Vector3D(-(borderWidth / 2f), app.height / 2f);
		PhysicsRectangle borderLeft = new PhysicsRectangle(pos, borderWidth,
				borderHeight, app, world, 0, 0, 0, scale);
		borderLeft.setName("borderLeft");
		parent.addChild(borderLeft);
		// Right border
		pos = new Vector3D(app.width + (borderWidth / 2), app.height / 2);
		PhysicsRectangle borderRight = new PhysicsRectangle(pos, borderWidth,
				borderHeight, app, world, 0, 0, 0, scale);
		borderRight.setName("borderRight");
		parent.addChild(borderRight);
		// Top border
		borderWidth = app.width;
		borderHeight = 50f;
		pos = new Vector3D(app.width / 2, -(borderHeight / 2));
		PhysicsRectangle borderTop = new PhysicsRectangle(pos, borderWidth,
				borderHeight, app, world, 0, 0, 0, scale);
		borderTop.setName("borderTop");
		parent.addChild(borderTop);
		// Bottom border
		pos = new Vector3D(app.width / 2, app.height + (borderHeight / 2));
		PhysicsRectangle borderBottom = new PhysicsRectangle(pos, borderWidth,
				borderHeight, app, world, 0, 0, 0, scale);
		borderBottom.setName("borderBottom");
		parent.addChild(borderBottom);
	}
}
