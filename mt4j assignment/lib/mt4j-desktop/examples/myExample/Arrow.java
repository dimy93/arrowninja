package myExample;

import org.jbox2d.common.Vec2;
import org.jbox2d.dynamics.Body;
import org.mt4j.components.visibleComponents.shapes.MTRectangle;
import org.mt4j.util.MTColor;
import org.mt4j.util.animation.Animation;
import org.mt4j.util.animation.AnimationEvent;
import org.mt4j.util.animation.IAnimationListener;
import org.mt4j.util.animation.MultiPurposeInterpolator;
import org.mt4j.util.math.Matrix;
import org.mt4j.util.math.Vector3D;

import advanced.physics.physicsShapes.PhysicsRectangle;

import processing.core.PApplet;
import processing.core.PImage;

public class Arrow extends MTRectangle {
	private float width = 1, height = 300f/Bow.scale;
	protected PApplet context;
	public Arrow(PApplet applet ) {
		super(applet, 0, 0, 1, 1);
		context = applet;
		final PImage arrowImg = applet.loadImage(BowScene.imagesPath+"arrow-new.png");
		float w = (arrowImg.width +0f) / arrowImg.height * height;
		float h = height;
		setVisible(true);
		setTextureEnabled(true);
		setStrokeColor(new MTColor(0, 0, 0, 0));
		setTexture(arrowImg);
		setSizeLocal(w, h);
		fadeIn();
		
	}
	
	public void fire(float speed){
		//Collision
		final PhysicsRectangle collidable = new PhysicsRectangle(new Vector3D(0,0), getWidth(), getHeight(), context, BowScene.world, 0f, 0f, 0f,  BowScene.scale);
		collidable.setName("arrow");
		collidable.setStrokeColor(new MTColor(0,0,0,0));
		BowScene.physicsContainer.addChild(collidable);
		collidable.setFillColor(new MTColor(0,0,0,0));
		
		final Bow parent = (Bow)getParent();
		final float current_y = parent.getCenterPointGlobal().y;
		final float current_x = parent.getCenterPointGlobal().x;
		
		Matrix m = parent.getGlobalMatrix().clone();
		m.mult(this.getLocalMatrix());

		final float rot = ((Bow)parent).getRotationZLocal();
		parent.removeChild(this);
		parent.getParent().addChild(this);

		this.rotateZGlobal(this.getCenterPointGlobal(), rot);
		setPositionRelativeToParent(new Vector3D(current_x,current_y,0));
		
		MultiPurposeInterpolator interpolator = new MultiPurposeInterpolator(
				current_y, current_y-1000, 600/speed, 1f, 1f, 1);
		final Animation moveAnimation = new Animation("move animation",
				interpolator, this, 0);
		moveAnimation.addAnimationListener(new IAnimationListener() {
			Vector3D direction = new Vector3D(current_x,0,0);
			final Vector3D initial = new Vector3D(current_x,current_y,0);
			@Override
			public void processAnimationEvent(AnimationEvent ae) {
				if (ae.getId() == ae.ANIMATION_ENDED)
				{
					getParent().removeChild(Arrow.this);
					collidable.getParent().removeChild(collidable);
				}
				direction.x = current_x;
				direction.y = ae.getValue();
				direction.z = 0;
				direction.rotateZ(initial, rot);
				setPositionRelativeToParent(direction);
				Body body = collidable.getBody();
				body.setXForm(
						new Vec2(Arrow.this.getCenterPointGlobal().x/BowScene.scale, Arrow.this.getCenterPointGlobal().y/BowScene.scale),
						(float)Math.toRadians(rot));
			}

		});
		moveAnimation.start();
	}
	
	protected void fadeIn() {
		MultiPurposeInterpolator fadeInInterpolator = new MultiPurposeInterpolator(
				0, 255, 3000, .2f, .8f, 1);
		final Animation fadeInAnimation = new Animation("fade in anim",
				fadeInInterpolator, this, 0);
		fadeInAnimation.addAnimationListener(new IAnimationListener() {

			@Override
			public void processAnimationEvent(AnimationEvent ae) {
				MTColor componentFillColor = getFillColor();
				componentFillColor.setAlpha(ae.getValue());
				setFillColor(componentFillColor);
			}
		});
		fadeInAnimation.start();
	}
	
	@Override
	public void setSizeLocal( float w, float h ){
		super.setSizeLocal(w, h);
		width = w;
		height = h;
	}
	
	public float getWidth(){
		return width;
	}
	public float getHeight(){
		return height;
	}
}
